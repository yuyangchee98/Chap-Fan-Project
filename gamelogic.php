<?php 

session_start();
$_SESSION["Answer"] = $_POST['ans']; 
//echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';

if ($_SESSION["Answer"] == $_SESSION["priceData"]) {
	echo "<script>location.href='../results/win.php';</script>";
	unset($_SESSION["Answer"]);
	session_destroy();
}
elseif ($_SESSION["Answer"] == null) {
	echo "<script>location.href='../results/noAns.php';</script>";
	unset($_SESSION["Answer"]);
	session_destroy();
	
}	
else {
	echo "<script>location.href='../results/lose.php';</script>";
	unset($_SESSION["Answer"]);
	session_destroy();
}
?>